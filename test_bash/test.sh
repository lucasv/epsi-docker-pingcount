#!/bin/bash
#####################################################
#count_test.sh                                       #
#                                                    #
# Script qui teste la bonne incrémentation du count  #
#                                                    #
# MàJ le 11/12/17 par Lucas Vagnier et Bryan Choisi  #
#                                                    #
####################################################

#read -p "Adresse du site : " ip_server  # Entrée utilisateur stockée dans la variable ip_server

echo "Test du serveur : $URL"
 
ip_server=$URL

# Condition permettant de tester le renvoie de la route /ping

if [ `curl -s "$ip_server/ping"  | jq .message` == "\"pong\"" ] 2>/dev/null  # Incrémente les compteurs si le serveur renvoie "pong"	
then
	echo -e "$ip_server : Serveur fonctionnel\n"

	compteur1=`curl -s "$ip_server/count" | jq .pingCount`
	echo "Valeur compteur avant test : $compteur1"
	
	curl -s "$ip_server/ping" &>/dev/null
	
	compteur2=`curl -s "$ip_server/count" | jq .pingCount`
	echo "Valeur compteur après test : $compteur2"

# Quitte le script si le serveur ne renvoie pas "pong"
else 
	echo -e "Erreur de réponse serveur\n	
Sortie du script ... "
	exit 1
fi

# Compare la valeur des compteur et valide l'incrémentation ou non

if [ $compteur2 -gt $compteur1 ]
then
	echo "Le compteur a bien été incrémenté"
else
	echo "Le compteur n'a pas changé"
	exit 1
fi

exit 0
