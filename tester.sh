#!/usr/bin/env bash
i=8
j=12
while [ "$i" -le "$j" ]; do
  docker-compose -f docker-compose-for-test-all-project.yml up -d group-$i-server
  sleep 10
  docker-compose -f docker-compose-for-test-all-project.yml up group-$i-test
  i=$(($i + 1))
done