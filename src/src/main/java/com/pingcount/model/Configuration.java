package com.pingcount.model;

import java.net.URI;

import javax.ws.rs.core.UriBuilder;

public class Configuration {
	
	private int PORT = 8080;
	private String SENTRY_DSN = "";
	
	public Configuration() {
		this.PORT = Integer.parseInt(System.getenv("SRV_PORT"));
		this.SENTRY_DSN = System.getenv("SENTRY_DSN");
	}
	
	public Configuration(int port, String DSN) {
		this.PORT = port;
		this.SENTRY_DSN = DSN;
	}
	
	public int getPort() {
		return this.PORT;
	}
	
	public URI getURI() {
		return UriBuilder.fromUri("http://0.0.0.0/").port(this.PORT).build();
	}
	
	public String getDSN() {
		return SENTRY_DSN;
	}
	
	public boolean isDSNEmpty() {
		if(SENTRY_DSN != null)
			return SENTRY_DSN.equals("");
		else
			return true;
	}
}
