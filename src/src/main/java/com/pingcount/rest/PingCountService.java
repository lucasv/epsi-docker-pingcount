package com.pingcount.rest;

import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pingcount.model.JSONMessage;
import com.pingcount.model.PingCount;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;

@Path("/")
public class PingCountService {

    private static PingCount PINGCOUNT = new PingCount(0);
    private ObjectMapper mapper = new ObjectMapper();
    
    
    public PingCountService() {
    	mapper.setVisibility(PropertyAccessor.ALL, Visibility.NONE);
    	mapper.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);
    }
    
    @GET
	@Path("/count")
	public Response getCount() throws Exception {
            
            String json = mapper.writeValueAsString(PINGCOUNT);

            return Response.status(200).entity(json).build();
	}

	@GET
	@Path("/ping")
	public Response getPing() {
            PINGCOUNT.increment();            
            JSONMessage m = new JSONMessage("pong");
            try {
            	
            
            return Response.status(200).entity(mapper.writeValueAsString(m)).build();
	
            }catch(Exception e) {
            	e.printStackTrace();
            }
            
            
            return Response.status(200).entity("").build();
            
         }
}