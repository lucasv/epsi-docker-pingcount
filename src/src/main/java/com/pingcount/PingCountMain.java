package com.pingcount;

import io.sentry.Sentry;
import java.io.File;
import java.io.IOException;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.annotation.JsonAutoDetect.Visibility;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.pingcount.model.Configuration;

import java.util.Scanner;

public class PingCountMain {

	/**
	 * Configuration class
	 */
	private static Configuration configuration;

	public static void main(String[] args) throws JsonParseException, JsonMappingException, IOException {

		try {
			
			// JSON Mapper
			ObjectMapper mapper = new ObjectMapper();
			mapper.setVisibility(PropertyAccessor.ALL, Visibility.NONE);
			mapper.setVisibility(PropertyAccessor.FIELD, Visibility.ANY);

			// Loading configuration by env vars
			configuration = new Configuration();
						
			// Logger sentry
			if(!configuration.isDSNEmpty()) {
				Sentry.init(configuration.getDSN()); 
			}

			ResourceConfig rc = new ResourceConfig();
			rc.packages("com.pingcount.rest");

			HttpServer server = GrizzlyHttpServerFactory.createHttpServer(configuration.getURI(), rc);
			server.start();            

			log("[PINGCOUNT] START : Port = " + configuration.getPort());
			
			System.out.println("Taper exit pour quitter");

			//create the Scanner
			Scanner terminalInput = new Scanner(System.in);

			//read input
			String user_input = "";

            while(!user_input.equals("exit")){
            	try{
            		user_input = terminalInput.nextLine();
            	}catch(Exception e){
            		Thread.sleep(1000);
            	}
                
            }
			
			
			
			server.shutdownNow();

			log("[PINGCOUNT] STOPPED"); 


		} catch (Exception e) {
			log(e);
			e.printStackTrace();
		}
	}
	
	public static void log(String msg) {
		System.out.println(msg);
		if(!configuration.isDSNEmpty()) {
			Sentry.capture(msg);
		}		
	}
	
	public static void log(Exception msg) {		
		System.out.println(msg);
		if(!configuration.isDSNEmpty()) {
			Sentry.capture(msg);
		}		
	}


}
